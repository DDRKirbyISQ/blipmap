/*
BlipMap
Timothy Wong
Music220A
Fall 2011

BlipMap is designed to be used as a command-line tool in conjunction with imgdumper.  You'll probably need the --caution-to-the-wind flag.  Run without args to see usage, or see below.
*/

// Prints usage instructions.
fun void PrintUsage() {
    <<<"Usage: chuck blipmap.ck:" +
        "[datafile]:[maxheight]:[frequency]:[length]:[envelope]:[mapping]:" + 
        "[scale]:[scaleamount]:[outfile]\n" +
        "\n" +
        "[datafile]\n" +
        "The path to an image file,\n" +
        "to be processed via imgdumper.\n" +
        "\n" +
        "[maxheight]\n" +
        "Maximum number of rows to analyze in the image.\n" +
        "-1 to use no maximum.\n" +
        "\n" +
        "[frequency]\n" +
        "Base/fundamental frequency for row 0.\n" +
        "\n" +
        "[length]\n" +
        "Duration, in seconds, of each column of pixels.\n" + 
        "\n" +
        "[envelope]\n" +
        "Duration, in seconds, of the volume envelope.\n" +
        "\n" +
        "[mapping]\n" +
        "rg - red maps to left channel, green maps to right channel\n" +
        "lum - mono, uses overall brightness only\n" +
        "delta - like rg, but uses abs(change in value) instead of value\n" +
        "\n" +
        "[scale]\n" +
        "linear - freq = base + y * [scaleamount]\n" +
        "powplus - freq = base + y^scaleamount\n" +
        "powmult - freq = base * y^scaleamount\n" +
        "expplus - freq = base + scaleamount^y\n" +
        "expmult - freq = base * scaleamount^y\n" +
        "blue - freq of each row is blue * scaleamount\n" +
        "\n" +
        "[scaleamount]\n" +
        "(See above)\n" +
        "\n" +
        "[outfile]\n" +
        "File to output to, with no .wav extension.\n" +
        "\n" +
        "Place imgdumper.exe in your current working directory.">>>;
}

// Print usage if no args were given.
// TODO: I don't know how to get the actual number of args given...
if (!me.args()) {
    PrintUsage();
    me.exit();
}

me.arg(0) => string inputPath;
Std.atoi(me.arg(1)) => int maxHeight;
Std.atof(me.arg(2)) => float frequency;
Std.atof(me.arg(3)) => float length;
Std.atof(me.arg(4)) => float envelope;
me.arg(5) => string mapping;
me.arg(6) => string scale;
Std.atof(me.arg(7)) => float scaleamount;
me.arg(8) => string outputPath;

// Run imgdumper.
Std.system("imgdumper " + inputPath + " data.txt");

// Open the text file.
FileIO inputFile;
inputFile.open("data.txt", FileIO.READ);
if (!inputFile.good()) {
    <<< "Error: couldn't open", inputPath, "for reading!">>>;
    me.exit();
}

// Read in the width and height.
inputFile => int width;
inputFile => int height;
if (maxHeight > 0 && height > maxHeight) {
    maxHeight => height;
}
<<<"Opened file" + inputPath + "with width=" + width + ", height=" + height>>>;

<<<"Reading...">>>;
// Read into pixel array.
int pixels[width][height][3];
for (0 => int y; y < height; 1 +=> y) {
    for (0 => int x; x < width; 1 +=> x) {
        for (0 => int component; component < 3; 1 +=> component) {
            // Reverse y axis so that 0 is on the bottom.
            inputFile => pixels[x][height-1-y][component];
        }
    }
}
<<<"Read " + width+height + " pixels">>>;

// Initialize sine oscs and envelopes.
SinOsc oscs[height][2];
Envelope envelopes[height][2];
for (0 => int y; y < height; 1 +=> y) {
    for (0 => int channel; channel < 2; 1 +=> channel) {
        oscs[y][channel] => envelopes[y][channel] => dac.chan(channel);
        envelopes[y][channel].value(0);
        envelopes[y][channel].duration(envelope::second);
    }
}

// Open output files for writing.
if (outputPath != "") {
    dac.chan(0) => WvOut waveOutL => blackhole;
    dac.chan(1) => WvOut waveOutR => blackhole;
    outputPath + "_l.wav" => waveOutL.wavFilename;
    outputPath + "_r.wav" => waveOutR.wavFilename;
}

fun void FrequencyUpdate(int x) {
    frequency => float currentFrequency;

    for (0 => int y; y < height; 1 +=> y) {
        for (0 => int channel; channel < 2; 1 +=> channel) {
            if (scale == "linear") {
                oscs[y][channel].freq(frequency + y * scaleamount);
            } else if (scale == "powplus") {
                oscs[y][channel].freq(frequency + Math.pow(y,scaleamount));
            } else if (scale == "powmult") {
                oscs[y][channel].freq(frequency * Math.pow(y,scaleamount));
            } else if (scale == "expplus") {
                oscs[y][channel].freq(frequency + Math.pow(scaleamount,y));
            } else if (scale == "expmult") {
                oscs[y][channel].freq(frequency * Math.pow(scaleamount,y));
            } else if (scale == "blue") {
                oscs[y][channel].freq(currentFrequency);
                pixels[x][y][2] * scaleamount +=> currentFrequency;
            }
        }
    }
}

// Playback.
for (0 => int x; x < width; 1 +=> x) {
    FrequencyUpdate(x);
    for (0 => int y; y < height; 1 +=> y) {
        if (mapping == "rg") {
            envelopes[y][0].target(pixels[x][y][0] / (255.0 * height));
            envelopes[y][1].target(pixels[x][y][1] / (255.0 * height));
        } else if (mapping == "lum") {
            pixels[x][y][0] + pixels[x][y][1] + pixels[x][y][2] => float target;
            (255 * 3 * height) /=> target;
            envelopes[y][0].target(target);
            envelopes[y][1].target(target);
        } else if (mapping == "delta") {
            float current;
            float before;
            pixels[x][y][0] => current;
            if (x == 0) {
                0 => before;
            } else {
                pixels[x-1][y][0] => before;
            }
            envelopes[y][0].target((current - before) / (255.0 * height));

            pixels[x][y][1] => current;
            if (x == 0) {
                0 => before;
            } else {
                pixels[x-1][y][1] => before;
            }
            envelopes[y][1].target((current - before) / (255.0 * height));
        }
    }

    length::second => now;
}

// Let sound die out.
for (0 => int y; y < height; 1 +=> y) {
    for (0 => int channel; channel < 2; 1 +=> channel) {
        envelopes[y][channel].target(0);
    }
}

envelope::second => now;